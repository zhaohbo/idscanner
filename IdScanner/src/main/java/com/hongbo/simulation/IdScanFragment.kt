package com.hongbo.simulation

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hongbo.idscanner.IdScan
import com.hongbo.idscanner.IdScanHelper.Companion.ins
import com.hongbo.idscanner.databinding.IdScanFragmentBinding

class IdScanFragment : Fragment() {

    companion object {
        fun newInstance() = IdScanFragment()
    }

    private lateinit var binding: IdScanFragmentBinding

    private val idReceiver = IDReceiver()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = IdScanFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ins!!.start(requireContext())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //添加身份证识别设备状态监听
        initFilterReceiver()

        //初始化按钮状态
        flushDeviceButton()
    }

    private fun initFilterReceiver() {
        val filter = IntentFilter()
        filter.addAction(IdScan.SCAN_DEVICE_ID_FILTER)
        requireActivity().registerReceiver(idReceiver, filter)
    }

    /**
     * 身份识别设备状态
     *
     * @param isShow
     * @param tips
     */
    fun flushDeviceInfo(isShow: Boolean, tips: String?) {
        if (isShow) {
            binding.scanTips.visibility = View.VISIBLE
            binding.scanTips.text = convertTips(tips)
            binding.buttonScan.visibility = View.VISIBLE
            binding.switchAuto.visibility = View.VISIBLE
            flushDeviceButton()
        } else {
            binding.progressBarDevice.visibility = View.GONE
            binding.scanTips.visibility = View.GONE
            binding.buttonScan.visibility = View.GONE
            binding.switchAuto.visibility = View.GONE
        }
    }

    private fun convertTips(tips: String?): String? {
        return if (tips != null && tips == "卡认证失败") {
            "请放置身份证件"
        } else tips
    }

    /**
     * 按钮状态
     */
    private fun flushDeviceButton() {
        if (ins!!.stop) {
            binding.buttonScan.text = "开始读卡"
            binding.buttonScan.isEnabled = true
            binding.progressBarDevice.visibility = View.GONE
            binding.buttonScan.setOnClickListener {
                if (binding.switchAuto.isChecked) {
                    binding.buttonScan.isEnabled = false
                    binding.scanTips.text = "读卡设备准备中"
                    binding.progressBarDevice.visibility = View.VISIBLE
                    ins!!.startAuto(true)
                } else {
                    ins!!.comRead()
                }
            }
        } else {
            if (ins!!.com) {
                binding.buttonScan.isEnabled = true
                binding.buttonScan.text = "停止读卡"
                binding.progressBarDevice.visibility = View.VISIBLE
                binding.buttonScan.setOnClickListener {
                    binding.buttonScan.isEnabled = false
                    binding.scanTips.text = "读卡设备停用中"
                    binding.progressBarDevice.visibility = View.VISIBLE
                    ins!!.startAuto(false)
                }
            } else {
                binding.buttonScan.isEnabled = false
                binding.buttonScan.text = "设备准备中"
                binding.progressBarDevice.visibility = View.VISIBLE
            }
        }
    }

    internal inner class IDReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == IdScan.SCAN_DEVICE_ID_FILTER) {
                val deviceTips = intent.getSerializableExtra(IdScan.KEY_DEVICE_INFO) as String?
                flushDeviceInfo(null != deviceTips && !TextUtils.isEmpty(deviceTips), deviceTips)
            }
        }
    }

    override fun onDestroy() {
        ins!!.close()
        requireActivity().unregisterReceiver(idReceiver)
        super.onDestroy()
    }

}