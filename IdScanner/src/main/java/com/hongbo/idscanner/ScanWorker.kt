package com.hongbo.idscanner

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters

/**
 * Created by HongboZhao on 2021/1/8.
 */
class ScanWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {
    override fun doWork(): Result {
        IdScanHelper.ins?.open()
        return Result.success()
    }
}