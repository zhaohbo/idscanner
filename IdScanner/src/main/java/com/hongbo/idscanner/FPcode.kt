package com.hongbo.idscanner

/**
 * 指纹数据
 * Created by HongboZhao on 2021/1/22.
 */
class FPcode(
        //指纹位置
        val fingerPosition: String?,
        //指纹质量
        val fingerprint: Int?
)