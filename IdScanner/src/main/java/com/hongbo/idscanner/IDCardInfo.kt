package com.hongbo.idscanner

import com.huashi.otg.sdk.HSIDCardInfo
import java.io.Serializable
import java.util.*

/**
 * 身份证信息
 * Created by HongboZhao on 2021/1/8.
 */
class IDCardInfo : Serializable {
    var certType: String? = null //证件类型 身份证,港澳台居住证（J）,外国人永久居留证（I）
    var peopleName: String? = null //姓名|英文名称
    var chineseName: String? = null //中文名称(I)
    var sex: String? = null //性别
    var people: String? = null //民族
    var birthDay: Date? = null //出生日期
    var addr: String? = null //地址( | J)
    var idCard: String? = null //身份号码|永久居留证号
    var department: String? = null //签发机关|申请受理机关
    var strartDate: String? = null //有效期限
    var endDate: String? = null //有效期限
    var uid: String? = null //uid
    var fpDate: ByteArray? = null //指纹
    var issuesNum: String? = null //签发次数(J)
    var passCheckID: String? = null //通行证号码(J)
    var strNationCode: String? = null //国籍(I)
    var strCertVer: String? = null //证件版本号(I)

    fun getFistPFInfo(): String {
        return if (fpDate != null && fpDate!![4] == 0x01.toByte()) {
            java.lang.String.format(
                "指纹  信息：第一枚指纹注册成功。指位：%s。指纹质量：%d",
                IdScan.GetFPcode(fpDate!![5].toInt()), fpDate!![6]
            )
        } else {
            "身份证无指纹"
        }
    }

    fun getSecondPFInfo(): String {
        return if (fpDate != null && fpDate!![512 + 4] == 0x01.toByte()) {
            java.lang.String.format(
                "指纹  信息：第二枚指纹注册成功。指位：%s。指纹质量：%d",
                IdScan.GetFPcode(fpDate!![512 + 5].toInt()), fpDate!![512 + 6]
            )
        } else {
            "身份证无指纹"
        }
    }

    fun parse(ic: HSIDCardInfo): IDCardInfo {
        this.certType = ic.getcertType()
        this.peopleName = ic.peopleName
        this.chineseName = ic.getstrChineseName()
        this.sex = ic.sex
        this.people = ic.people
        this.birthDay = ic.birthDay
        this.addr = ic.addr
        this.idCard = ic.idCard
        this.department = ic.department
        this.strartDate = ic.strartDate
        this.endDate = ic.endDate
        this.uid = ic.uid
        this.fpDate = ic.fpDate
        this.issuesNum = ic.getissuesNum()
        this.passCheckID = ic.passCheckID
        this.strNationCode = ic.getstrNationCode()
        this.strCertVer = ic.getstrCertVer()
        return this
    }

    fun parseInput(idCard: String): IDCardInfo {
        this.idCard = idCard
        return this
    }
}