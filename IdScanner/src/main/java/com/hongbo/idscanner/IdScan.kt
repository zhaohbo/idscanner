package com.hongbo.idscanner

/**
 * Created by HongboZhao on 2021/1/8.
 */
object IdScan {

    const val SCAN_FILTER = "com.hongbo.idscanner"
    const val KEY_ID_SCAN_INFO = "key_id_scan_info"
    const val DELAY_TIME = 1000L

    //设备状态广播
    const val SCAN_DEVICE_ID_FILTER = "com.hongbo.id.device"
    const val KEY_DEVICE_INFO = "key_device_info"

    const val SCAN_DEVICE_ID_IMG_FILTER = "com.hongbo.id.img"
    const val KEY_DEVICE_IMG_INFO = "key_device_img_info"

    /**
     * 指纹 指位代码
     *
     * @param FPcode
     * @return
     */
    fun GetFPcode(FPcode: Int): String? {
        return when (FPcode) {
            11 -> "右手拇指"
            12 -> "右手食指"
            13 -> "右手中指"
            14 -> "右手环指"
            15 -> "右手小指"
            16 -> "左手拇指"
            17 -> "左手食指"
            18 -> "左手中指"
            19 -> "左手环指"
            20 -> "左手小指"
            97 -> "右手不确定指位"
            98 -> "左手不确定指位"
            99 -> "其他不确定指位"
            else -> "未知"
        }
    }

    // 将逗号分隔的字符串转换为byte数组
    fun String2byte(b: ByteArray, StrBuf: String): Int {
        val parts = StrBuf.split(",").toTypedArray()
        var Itmp: Int
        val Len = parts.size
        if (Len == b.size) {
            for (i in 0 until Len) {
                try {
                    Itmp = Integer.valueOf(parts[i], 16)
                    b[i] = Itmp.toByte()
                } catch (e: NumberFormatException) {
                    e.printStackTrace()
                }
            }
            return Len
        }
        return -1
    }

    private val NationName = arrayOf(
            "阿富汗", "奥兰群岛", "阿尔巴尼亚", "阿尔及利亚", "美属萨摩亚", "安道尔", "安哥拉", "安圭拉",
            "南极洲", "安提瓜和巴布达", "阿根廷", "亚美尼亚", "阿鲁巴", "澳大利亚", "奥地利", "阿塞拜疆",
            "巴哈马", "巴林", "孟加拉国", "巴巴多斯", "白俄罗斯", "比利时", "伯利兹", "贝宁", "百慕大",
            "不丹", "玻利维亚", "波黑", "博茨瓦纳", "布维岛", "巴西", "英属印度洋领地", "文莱", "保加利亚",
            "布基纳法索", "布隆迪", "柬埔寨", "喀麦隆", "加拿大", "佛得角", "开曼群岛", "中非", "乍得", "智利",
            "中国", "圣诞岛", "科科斯（基林）群岛", "哥伦比亚", "科摩罗", "刚果（布）", "刚果（金）", "库克群岛",
            "哥斯达黎加", "科特迪瓦", "克罗地亚", "古巴", "塞浦路斯", "捷克", "丹麦", "吉布提", "多米尼克",
            "多米尼加", "厄瓜多尔", "埃及", "萨尔瓦多", "赤道几内亚", "厄立特里亚", "爱沙尼亚", "埃塞俄比亚",
            "福克兰群岛（马尔维纳斯）", "法罗群岛", "斐济", "芬兰", "法国", "法属圭亚那", "法属波利尼西亚",
            "法属南部领地", "加蓬", "冈比亚", "格鲁吉亚", "德国", "加纳", "直布罗陀", "希腊", "格陵兰",
            "格林纳达", "瓜德罗普", "关岛", "危地马拉", "格恩西岛", "几内亚", "几内亚比绍", "圭亚那", "海地",
            "赫德岛和麦克唐纳岛", "梵蒂冈", "洪都拉斯", "香港", "匈牙利", "冰岛", "印度", "印度尼西亚", "伊朗",
            "伊拉克", "爱尔兰", "英国属地曼岛", "以色列", "意大利", "牙买加", "日本", "泽西岛", "约旦",
            "哈萨克斯坦", "肯尼亚", "基里巴斯", "朝鲜", "韩国", "科威特", "吉尔吉斯斯坦", "老挝", "拉脱维亚",
            "黎巴嫩", "莱索托", "利比里亚", "利比亚", "列支敦士登", "立陶宛", "卢森堡", "澳门", "前南马其顿",
            "马达加斯加", "马拉维", "马来西亚", "马尔代夫", "马里", "马耳他", "马绍尔群岛", "马提尼克",
            "毛利塔尼亚", "毛里求斯", "马约特", "墨西哥", "密克罗尼西亚联邦", "摩尔多瓦", "摩纳哥", "蒙古",
            "黑山", "蒙特塞拉特", "摩洛哥", "莫桑比克", "缅甸", "纳米比亚", "瑙鲁", "尼泊尔", "荷兰",
            "荷属安的列斯", "新喀里多尼亚", "新西兰", "尼加拉瓜", "尼日尔", "尼日利亚", "纽埃", "诺福克岛",
            "北马里亚纳", "挪威", "阿曼", "巴基斯坦", "帕劳", "巴勒斯坦", "巴拿马", "巴布亚新几内亚", "巴拉圭",
            "秘鲁", "菲律宾", "皮特凯恩", "波兰", "葡萄牙", "波多黎各", "卡塔尔", "留尼汪", "罗马尼亚",
            "俄罗斯联邦", "卢旺达", "圣赫勒拿", "圣基茨和尼维斯", "圣卢西亚", "圣皮埃尔和密克隆",
            "圣文森特和格林纳丁斯", "萨摩亚", "圣马力诺", "圣多美和普林西比", "沙特阿拉伯", "塞内加尔",
            "塞尔维亚", "塞舌尔", "塞拉利昂", "新加坡", "洛伐克", "斯洛文尼亚", "所罗门群岛", "索马里", "南非",
            "南乔治亚岛和南桑德韦奇岛", "西班牙", "斯里兰卡", "苏丹", "苏里南", "斯瓦尔巴岛和扬马延岛",
            "斯威士兰", "瑞典", "瑞士", "叙利亚", "台湾", "塔吉克斯坦", "坦桑尼亚", "泰国", "东帝汶", "多哥",
            "托克劳", "汤加", "特立尼达和多巴哥", "突尼斯", "土耳其", "土库曼斯坦", "特克斯和凯科斯群岛",
            "图瓦卢", "乌干达", "乌克兰", "阿联酋", "英国", "美国", "美国本土外小岛屿", "乌拉圭",
            "乌兹别克斯坦", "瓦努阿图", "委内瑞拉", "越南", "英属维尔京群岛", "美属维尔京群岛", "瓦利斯和富图纳",
            "西撒哈拉", "也门", "赞比亚", "津巴布韦"
    )
    private val NationTwoNationCodes = arrayOf(
            "AF", "AX", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG",
            "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB",
            "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BA", "BW", "BV",
            "BR", "IO", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV",
            "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CG",
            "CD", "CK", "CR", "CI", "HR", "CU", "CY", "CZ", "DK", "DJ",
            "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK",
            "FO", "FJ", "FI", "FR", "GF", "PF", "TF", "GA", "GM", "GE",
            "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GG",
            "GN", "GW", "GY", "HT", "HM", "VA", "HN", "HK", "HU", "IS",
            "IN", "ID", "IR", "IQ", "IE", "IM", "IL", "IT", "JM", "JP",
            "JE", "JO", "KZ", "KE", "KI", "KP", "KR", "KW", "KG", "LA",
            "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK",
            "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU",
            "YT", "MX", "FM", "MD", "MC", "MN", "ME", "MS", "MA", "MZ",
            "MM", "NA", "NR", "NP", "NL", "AN", "NC", "NZ", "NI", "NE",
            "NG", "NU", "NF", "MP", "NO", "OM", "PK", "PW", "PS", "PA",
            "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RE",
            "RO", "RU", "RW", "SH", "KN", "LC", "PM", "VC", "WS", "SM",
            "ST", "SA", "SN", "RS", "SC", "SL", "SG", "SK", "SI", "SB",
            "SO", "ZA", "GS", "ES", "LK", "SD", "SR", "SJ", "SZ", "SE",
            "CH", "SY", "TW", "TJ", "TZ", "TH", "TL", "TG", "TK", "TO",
            "TT", "TN", "TR", "TM", "TC", "TV", "UG", "UA", "AE", "GB",
            "US", "UM", "UY", "UZ", "VU", "VE", "VN", "VG", "VI", "WF",
            "EH", "YE", "ZM", "ZW"
    )
    private val NationThreeNationCodes = arrayOf(
            "AFG", "ALA", "ALB", "DZA", "ASM", "AND", "AGO", "AIA", "ATA", "ATG",
            "ARG", "ARM", "ABW", "AUS", "AUT", "AZE", "BHS", "BHR", "BGD", "BRB",
            "BLR", "BEL", "BLZ", "BEN", "BMU", "BTN", "BOL", "BIH", "BWA", "BVT",
            "BRA", "IOT", "BRN", "BGR", "BFA", "BDI", "KHM", "CMR", "CAN", "CPV",
            "CYM", "CAF", "TCD", "CHL", "CHN", "CXR", "CCK", "COL", "COM", "COG",
            "COD", "COK", "CRI", "CIV", "HRV", "CUB", "CYP", "CZE", "DNK", "DJI",
            "DMA", "DOM", "ECU", "EGY", "SLV", "GNQ", "ERI", "EST", "ETH", "FLK",
            "FRO", "FJI", "FIN", "FRA", "GUF", "PYF", "ATF", "GAB", "GMB", "GEO",
            "DEU", "GHA", "GIB", "GRC", "GRL", "GRD", "GLP", "GUM", "GTM", "GGY",
            "GIN", "GNB", "GUY", "HTI", "HMD", "VAT", "HND", "HKG", "HUN", "ISL",
            "IND", "IDN", "IRN", "IRQ", "IRL", "IMN", "ISR", "ITA", "JAM", "JPN",
            "JEY", "JOR", "KAZ", "KEN", "KIR", "PRK", "KOR", "KWT", "KGZ", "LAO",
            "LVA", "LBN", "LSO", "LBR", "LBY", "LIE", "LTU", "LUX", "MAC", "MKD",
            "MDG", "MWI", "MYS", "MDV", "MLI", "MLT", "MHL", "MTQ", "MRT", "MUS",
            "MYT", "MEX", "FSM", "MDA", "MCO", "MNG", "MNE", "MSR", "MAR", "MOZ",
            "MMR", "NAM", "NRU", "NPL", "NLD", "ANT", "NCL", "NZL", "NIC", "NER",
            "NGA", "NIU", "NFK", "MNP", "NOR", "OMN", "PAK", "PLW", "PSE", "PAN",
            "PNG", "PRY", "PER", "PHL", "PCN", "POL", "PRT", "PRI", "QAT", "REU",
            "ROU", "RUS", "RWA", "SHN", "KNA", "LCA", "SPM", "VCT", "WSM", "SMR",
            "STP", "SAU", "SEN", "SRB", "SYC", "SLE", "SGP", "SVK", "SVN", "SLB",
            "SOM", "ZAF", "SGS", "ESP", "LKA", "SDN", "SUR", "SJM", "SWZ", "SWE",
            "CHE", "SYR", "TWN", "TJK", "TZA", "THA", "TLS", "TGO", "TKL", "TON",
            "TTO", "TUN", "TUR", "TKM", "TCA", "TUV", "UGA", "UKR", "ARE", "GBR",
            "USA", "UMI", "URY", "UZB", "VUT", "VEN", "VNM", "VGB", "VIR", "WLF",
            "ESH", "YEM", "ZMB", "ZWE"
    )

    //串口
    private val ports = arrayOf(
            "/dev/ttyS0", "/dev/ttyS1", "/dev/ttyS2", "/dev/ttyS3", "/dev/ttyS4", "/dev/ttysWK0",
            "/dev/ttysWK1", "/dev/ttysWK2", "/dev/ttysWK3", "/dev/ttyMT0", "/dev/ttyMT1",
            "/dev/ttyMT2", "/dev/ttyMT3", "/dev/ttyHS0", "/dev/ttyHS1", "/dev/ttyHS2",
            "/dev/ttyHS3", "/dev/ttyHSL0", "/dev/ttyHSL1", "/dev/ttyHSL2", "/dev/ttyHSL3"
    )

    //波特率
    private val dauds = arrayOf(
            "115200", "9600"
    )

    private val voltages = arrayOf(
            "1.8V", "3V", "5V"
    )

    private fun getTwoNationCodes(str: String): String? {
        for (i in NationThreeNationCodes.indices) {
            if (str == NationThreeNationCodes[i]) {
                return NationTwoNationCodes[i]
            }
        }
        return ""
    }

    private fun getNationName(str: String): String? {
        for (i in NationThreeNationCodes.indices) {
            if (str == NationThreeNationCodes[i]) {
                return NationName[i]
            }
        }
        return ""
    }

    private var portItem = 0
    fun getPort(): String {
//        portItem++
//        if (portItem > ports.size - 1) {
//            portItem = 0
//        }
        return ports[portItem]
    }
}