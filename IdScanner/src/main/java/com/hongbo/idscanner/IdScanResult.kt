package com.hongbo.idscanner

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.huashi.otg.sdk.GetImg
import com.huashi.otg.sdk.HSIDCardInfo
import java.io.File
import java.io.FileInputStream
import java.text.SimpleDateFormat

/**
 * 结果处理
 * Created by HongboZhao on 2021/1/8.
 */
object IdScanResult {

    var df: SimpleDateFormat = SimpleDateFormat("yyyy年MM月dd日") // 设置日期格式

    /**
     * 提取指纹
     */
    fun getFPcode(ic: HSIDCardInfo): ArrayList<FPcode> {
        val pfList = ArrayList<FPcode>()
        var fp = ic.fpDate
        pfList.add(if (fp[4] == 0x01.toByte()) {
            FPcode(IdScan.GetFPcode(fp[5].toInt()), fp[6].toInt())
        } else {
            FPcode(null, null)
        })
        pfList.add(if (fp[512 + 4] == 0x01.toByte()) {
            FPcode(IdScan.GetFPcode(fp[512 + 5].toInt()), fp[512 + 6].toInt())
        } else {
            FPcode(null, null)
        })
        return pfList
    }

    val headerImgPath: String by lazy {
        (IdScanHelper.context?.filesDir?.absolutePath ?: "") + "/idImg.bmp"
    }

    /**
     * 获取生成后的头像
     */
    @JvmStatic
    fun getHeaderImg(): Bitmap {
        var fis = FileInputStream(File(headerImgPath))
        var bitmapHead = BitmapFactory.decodeStream(fis)
        fis.close()
        return bitmapHead
    }

    /**
     * 获取身份证背景图
     */
    @JvmStatic
    fun getIdBg(): Bitmap {
        var fis = FileInputStream(File(IdScanHelper.context!!.filesDir.absolutePath + File.separator + "idcardF.bmp"))
        var bitmapHead = BitmapFactory.decodeStream(fis)
        fis.close()
        return bitmapHead
    }

    /**
     * 生成正反面照片
     */
    fun parseImageBg(ic: HSIDCardInfo, bmpBuf: ByteArray): Bitmap {
        var bitmap = BitmapFactory.decodeByteArray(bmpBuf, 0, bmpBuf.size)
        var imgBg = GetImg.ShowBmp(ic, IdScanHelper.context, 1, IdScanHelper.context?.filesDir?.absolutePath, bitmap)
        bitmap.recycle()
        return imgBg
    }

    fun doResult(ic: HSIDCardInfo) {
        if (ic.getcertType() === " ") {
            Log.d(
                    "证件类型：身份证,姓名：${ic.peopleName},性别：${ic.sex}," +
                            "民族：${ic.people},出生日期：${df.format(ic.birthDay)},地址：${ic.addr}," +
                            "身份号码：${ic.idCard},签发机关：${ic.department}," +
                            "有效期限：${ic.strartDate}-${ic.endDate}," +
                            "UID：${ic.uid}"
            )
        } else {
            if (ic.getcertType() === "J") {
                Log.d(
                        "证件类型：港澳台居住证（J）,姓名：${ic.peopleName},性别：${ic.sex}," +
                                "签发次数：${ic.getissuesNum()},通行证号码：${ic.passCheckID}," +
                                "出生日期：${df.format(ic.birthDay)},地址：${ic.addr}," +
                                "身份号码：${ic.idCard},签发机关：${ic.department}," +
                                "有效期限：${ic.strartDate}-${ic.endDate}," +
                                "UID：${ic.uid}"
                )
            } else {
                if (ic.getcertType() === "I") {
                    Log.d(
                            "证件类型：外国人永久居留证（I）,英文名称：${ic.peopleName}," +
                                    "中文名称：${ic.getstrChineseName()},性别：${ic.sex}," +
                                    "永久居留证号：${ic.idCard},国籍：${ic.getstrNationCode()}," +
                                    "出生日期：${df.format(ic.birthDay)},证件版本号：${ic.getstrCertVer()}," +
                                    "申请受理机关：${ic.department}," +
                                    "有效期限：${ic.strartDate}-${ic.endDate}," +
                                    "UID：${ic.uid}"
                    )
                }
            }
        }
    }
}