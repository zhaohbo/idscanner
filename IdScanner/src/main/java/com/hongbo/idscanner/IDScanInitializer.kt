package com.hongbo.idscanner

import android.content.Context
import androidx.startup.Initializer
import com.hongbo.idscanner.IdScanHelper

/**
 * Created by HongboZhao on 2021/01/08.
 */
class IDScanInitializer : Initializer<Unit> {
    override fun create(context: Context) {
        IdScanHelper.context = context
    }

    override fun dependencies(): MutableList<Class<out Initializer<*>>> {
        return ArrayList()
    }

}