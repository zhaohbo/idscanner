package com.hongbo.idscanner

import com.zhb.tools.ZLog

/**
 * Created by HongboZhao on 2021/1/8.
 */
object Log {
    fun d(msg: String) {
        if (IdScanHelper.isDebug) {
            ZLog.setMethodCount(0)
            ZLog.e(msg)
        }
    }
}