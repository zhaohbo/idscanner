package com.hongbo.utils

import android.media.MediaPlayer
import com.hongbo.idscanner.IdScanHelper
import com.hongbo.idscanner.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.io.IOException

/**
 * Created by HongboZhao on 2021/1/12.
 */
object Player {
    fun playMp3() {
        runBlocking(Dispatchers.IO) {
            // 获取mp3文件的路径
            val mediaPlayer = MediaPlayer()
            if (IdScanHelper.context != null) {
                val file = IdScanHelper.context!!.resources.openRawResourceFd(R.raw.readsuss)
                try {
                    mediaPlayer.setDataSource(
                        file.fileDescriptor, file.startOffset,
                        file.length
                    )
                    mediaPlayer.prepare()
                    mediaPlayer.start()
                    delay(mediaPlayer.duration.toLong())
                    mediaPlayer.stop()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                mediaPlayer.release()
            }
        }
    }
}