### ID卡读卡器

##### 使用说明

####### 启用扫描
```kotlin
  IdScanHelper.ins?.start(this)
``` 

####### 关闭扫描
```kotlin
IdScanHelper.ins?.close()
```

####### 注册广播
```kotlin
var filter = IntentFilter(SCAN_FILTER)
registerReceiver(idReceiver, filter)
//注销广播
unregisterReceiver(idReceiver)
//广播接收
private val idReceiver by lazy { IDReceiver() }
private inner class IDReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == SCAN_FILTER) {
            var value = intent.getSerializableExtra(KEY_ID_SCAN_INFO)
            binding.tvInfo.text = Gson().toJson(value)
        }
    }
}
```